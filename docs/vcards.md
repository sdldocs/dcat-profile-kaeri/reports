> **Note**
>
> 이 페이지는 [vCard](https://www.techtarget.com/whatis/definition/vCard)를 편역하였다.

## vCard란?
v카드는 전자 명함 또는 개인 카드이며, 명함이나 개인 카드로 이루어지는 통신 교환의 종류에 대한 업계 사양의 이름이기도 하다. 누군가 보낸 이메일 메모에 첨부된 vCard를 본 적이 있을 것이다.

vCard에 포함된 정보에는 일반적으로 연락처 정보, 전화번호, 이메일 등 개인에 대한 [personal data](https://www.techtarget.com/searchsecurity/definition/personally-identifiable-information-PII)와 발신자가 원하는 경우 주민등록번호 또는 건강 데이터와 같은 기타 관련 데이터가 포함된다. 아래 그림은 일반적인 vCard를 보인다.

![](./images/vcard-h.png)

## vCard가 중요한 이유?
vCard는 공개된 업계 사양이므로 [소프트웨어 어플리케이션 개발자](https://www.techtarget.com/whatis/definition/software-development)는 수신자가 vCard를 볼 수 있도록 처리하거나 사용자가 주소록이나 다른 어플리케이션으로 끌어서 놓을 수 있는 프로그램을 만들 수 있다. vCard에는 텍스트뿐만 아니라 이미지와 오디오 클립도 포함될 수 있다.

## vCard의 배경
변Variant Call Format 파일 형식으로 저장되는 vCard는 Apple, AT&T, IBM 및 Siemens가 설립한 컨소시엄에서 개발했으며, 이 컨소시엄은 1996년에 사양을 업계 그룹인 인터넷 메일 컨소시엄(IMC)에 넘겼다. vCard 사양은 International Telegraph and Telephone Consultative Committee(현 Telecommunication Standardization Sector)의 디렉터리 서비스에 대한 X.500 시리즈 권장 사항에서 정의한 "Person" 객체를 사용하며, 그 확장으로 간주할 수 있다. vCard 형식에는 이름, 주소 정보, 날짜 및 시간, 선택적으로 사진, 회사 로고, 사운드 클립 및 지리 정보를 포함한다.

vCard 형식은 RFC 6350에 자세히 설명되어 있다.[ITF(Internet Engineering Task Force)](https://www.techtarget.com/whatis/definition/IETF-Internet-Engineering-Task-Force)이 형식을  개발했으며, Internet Engineering Steering Group에서 승인하여 2011년 8월에 발표되었다.

## vCard가 사용되는 방법
다른 사람이 이메일에 첨부한 vCard를 열려면 이메일 프로그램이 vCard를 지원해야 하지만 모든 프로그램이 지원하는 것은 아니다. 하지만 온라인 주소록에 vCard를 지원하는 프로그램이 있는 경우에는 해당 프로그램으로 vCard 파일을 이동하여 보거나 해당 프로그램의 데이터베이스에 추가할 수 있다. 받은 vCard를 열 수 없는 경우 해당 정보가 이메일의 다른 곳에 반복되어 있을 수 있다는 점을 기억하시오. 기본적으로 명함이다.

vCard의 친숙한 변형은 흰색 배경에 검은색 사각형 패턴이 있는 정사각형 이미지로 구성된 QR 코드 형식이다. 각 패턴은 다르며 스마트폰으로 스캔하면 QR 코드에 vCard 정보뿐만 아니라 보안 시스템의 액세스 코드와 같은 다양한 유형의 데이터가 표시된다. 
