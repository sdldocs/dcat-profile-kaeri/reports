# Data Management for KAERI Project

## 원자력 발전소 운전 데이터의 data management system을 위한 Catalog 구축

- [DCAT](dcat-intro.md)
- [DCAT-AP](dcat-ap.md)
- [DCAT-AP-JRC](dcat-ap-jrc.md)
- [DCAT-AP-KR](dcat-ap-kr.md)
- [DCAT-AP-KRXXX](dcat-ap-krxxx.md)

## References
- [Getting started with schema.org using Microdata](schema-org-start.md)
- [vCard](vcard.md)
- [FOAF](foaf.md)
- [FOAF Vocabulary Specification](foaf-voc-spec.md)
- [INFO216](https://wiki.app.uib.no/info216/index.php?title=INFO216_Wiki)