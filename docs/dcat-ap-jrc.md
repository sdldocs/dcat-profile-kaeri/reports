## Background
Metadata play a fundamental role, and are meant to address a number of requirements, which include (a) ensuring data documentation and inventory, (b) enabling data discovery and (c) publishing metadata on other catalogues operated by EU institutions and bodies

The design of the JRC metadata schema is following a modular approach consisting of a core profile, defining the elements that should be common to all metadata records, and a set of domain-specific extensions.

The reference metadata standard used is the DCAT application profile for European data portals (DCAT-AP) [DCAT-AP], and the related domain-specific extensions - namely, [GeoDCAT-AP] (for geospatial metadata) and [StatDCAT-AP] (for statistical metadata).

The core profile of the JRC metadata schema (referred to as DCAT-AP-JRC) is however not using DCAT-AP as is, but it extends it with a number of metadata elements that have been identified as most relevant across scientific domains, and which are required in order to support data citation.

## Methodology

### Metadata scope
DCAT-AP-JRC classes and properties are annotated also with the "scope(s)" where their use is relevant. The used "metadata scopes" are the following ones:

- Data documentation
- Data citation
- Metadata management
- Metadata sharing and re-use

## Requirements

### Requirements for scientific data

#### Metadata elements relevant across disciplines
The most common, cross-domain requirements we identified for JRC data are following ones:

1. Ability to indicate dataset authors.
2. Ability to describe data lineage.
3. Ability to give potential data consumers information on how to use the data ("usage notes").
4. Ability to link to (scientific) publications about a dataset.
5. Ability to link to input data (i.e., data used to create a dataset).

Points (2) (data lineage) and (5) (input data) are already supported by DCAT-AP via, respectively, dct:provenance and dct:source. The other ones are instead not supported by [DCAT-AP].

#### Data citation
DataCite is an international initiative meant to enable citation for scientific datasets. To achieve this, DataCite operates a metadata infrastructure, following the same approach used by CrossRef for scientific publications. As such, the DataCite infrastructure is responsible for issuing persistent identifiers (in particular, DOIs) for datasets, and for registering dataset metadata. Such metadata are to be provided according to the DataCite metadata schema [DataCite] – which is basically an extension to the one used for DOI records.

In addition to this, data citation requirements have been taken into account to determine which resource properties are mandatory, as they are essential to create a bibliographic citation. More precisely, these properties are the following ones:

- Contributor / author (see 10.2.9 Property: contributor).
- Title (see 10.2.2 Property: title).
- Publisher (see 10.2.11 Property: publisher).
- Publication date (see 10.2.4 Property: publication date).
- Identifiers for both the dataset (10.2.1 Property: identifier and 10.2.12 Property: other identifier) and the contributors (10.4.1.5 Property: other identifier).

#### Contributors' / authors' order
DCAT-AP-JRC adopts a simple mechanism, which is meant to record the contributors' / authors' order by listing them as a formatted string, and in addition specifies each single contributor / author as a separate resource.

### Additional requirements

#### Dataset collections
The possibility of grouping the datasets of a catalogue into subsets is already supported in [DCAT-AP], via the notion of "sub-catalogue". Therefore, DCAT-AP-JRC follows the same approach for modelling collections.

#### Catalogue endpoints
it is important to have information about these harvesting endpoints, their supported metadata schemas and protocols in order to automise the harvesting and metadata transformation procedures. For this reason, DCAT-AP-JRC includes the notion of "catalogue endpoint" ("endpoint", for short), which is modelled as both a catalogue (to denote the set of metadata available from it) and a service / API. Moreover, each endpoint is linked to the relevant catalogue and collection.

#### Dataset-catalogue relationship
When metadata records are harvested across catalogues, one of the important pieces of information is the original catalogue from which a record has been harvested. [DCAT-AP] and [VOCAB-DCAT] include a relationship, namely, dcat:dataset, to link a catalogue to the documented datasets, but this information will be lost when a dataset record is harvested in another catalogue. In order to address this issue, DCAT-AP-JRC makes use of property dct:isPartOf as the inverse property of dcat:dataset. Notably, dct:isPartOf is the inverse property of dct:hasPart, which is in turn the super-property of dcat:dataset.


#### Dataset quality conformance
The conformance of a dataset with given quality criteria is an important piece of information to understand its fit for purpose, as well as the rules followed for data management.

#### "Other" dataset-related resources

#### Identifiers
For simplicity, it is possible to group identifiers into two main types:

- Internal / primary identifiers, which are meant to identify a resource internally to a given catalogue, and are also important in federated harvesting scenarios as they are a means to specify from which catalogue they have been harvested (this especially when such identifiers are encoded as dereferenceable URIs).
- Additional / secondary identifiers, assigned by existing registers. E.g., the scientific community makes wide use of different (possibly persistent) identifiers, especially for publications, but now increasingly for authors and data. Including in metadata the (possibly multiple) persistent identifier(s) of a resouce is important not only for citation purposes, but also to unambiguously link related resources.

More precisely:

- Identifiers are encoded as HTTP URIs, whenever possible, or URNs, using owl:sameAs for URIs concerning additional / secondary identifiers.
- In addition:
    - Internal / primary identifiers are specified, as literals, with dct:identifier.
    - Additional / secondary identifiers are specified, as literals, with adms:identifier.

#### Agent roles

#### Short names / acronyms
Some resource types are frequently referred to by using an acronym rather than their full "name", which act also as a human-targeted / mnemonic identifier. For the same reason, and also because their "length" is more fit for user and administration interfaces, such acronyms / short names are also used as the "primary" name of a resource in data management tools and catalogues.

DCAT-AP-JRC uses property dct:alternative for specifying short names / acronyms for resources as catalogues, collections, and endpoints.

#### Spatial coverage code lists

#### Dataset status
DCAT-AP-JRC extends [DCAT-AP] by supporting the specification of the dataset status by using the [MDR-DS] code list. For more details, see 10.2.13 Property: status.

#### Distribution access restrictions
DCAT-AP-JRC supports the specification of access restrictions at the distribution level.

The identified types of access restrictions are the following ones:

- No limitations
- Registration required
- Authorisation required
The distribution can be accessed only by authorized users.

#### Distribution types and functions
It is often the case that distributions do not point to a file or a download page, but rather to a service / API from which users can access the data. Examples include SPARQL endpoints and services like [WMS], [WFS], etc., typically used for geospatial data. The result is that users are returned resources (as machine- or human-readable description of the service / API interface) providing no direct means to access the data. Morever, using these services / APIs may require specific expertise and/or software tools.

#### Improving data discoverability on the Web
more and more data catalogues are now taking care not only of documenting data, but also of optimising their publication in order to make them discoverable by mainstream search engines. One of the techniques used for this purpose is to embed metadata in Web pages, with mechanisms as [HTML-RDFa], [MICRODATA], [JSON-LD] (see § 6.20 Embedding JSON-LD in HTML Documents), and by using vocabularies as [DCTERMS], [VOCAB-DCAT], and [SCHEMA-ORG] - which has been specifically developed for this purpose.

## Namespaces

## Conformance

## Vocabulary overview
![Fig. 1](images/dcat-ap-jrc/dcat-ap-jrc-fig01.svg)

## Vocabulary specification
The following sections define the class and properties used in DCAT-AP-JRC. More precisely, after providing an overview of the classes used in DCAT-AP-JRC (9. Classes), class definitions and the corresponding properties are grouped as follows:

- Mandatory classes
- Recommended classes
- Optional classes
- Secondary 

The tables in the following sections include these columns:

- Column Obl. ("Obligation"), denotes whether the class / property is mandatory ("M"), recommended ("R"), optional ("O"), or conditional (C) - where conditional means "mandatory if given conditions are met".
- Column MC specifies the maximum cardinality of the corresponding property (the minimum cardinality is always 1 for all mandatory properties, and 0 for the other ones). Properties having as range a plain literal, can be specified multiple times, even though their max cardinality is 1, provided that each instance uses a different language tag. These case is marked by using an asterisk after the max cardinality value (e.g., "1*").
- Column Scope indicates the metadata scope(s) (see 3.3 Metadata scope) for which a specific class or property is relevant. The supported scopes are: data documentation (DD), data citation (DC), metadata management (MM), and metadata sharing and re-use (MS). When a class or property is relevant to all scopes, keyword "All" is used.
- Column Spec. ("Specification") indicates the metadata specification (namely, [DCAT-AP], [GeoDCAT-AP], [StatDCAT-AP], or DCAT-AP-JRC) where the use of a specific class or property is first defined.

## Classes

### Primary classes

- Catalogue (M)
- Dataset (M)
- Contact point (M)
- Agent (Individual/Organization) (M)
- Distribution (R)
- Publication (R)
- *Endpoint* (O)
- *Collection* (O)
- Catalogue record (O)
- *Other recource* (O)

### Secondary classes
- Lineage
- Usage notes
- Identifier
- Spatial coverage (as geoname/gepmetry)
- Temporal coverage

## Mandatory classes

### Class: Catalogue

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| title | M | 1* | dct:title | rdfs:Literal | All | DCAT-AP |
| description | M | 1* | dct:description | rdfs:Literal | All | DCAT-AP |
| contact point | M | 1 | dcat:contactPoint | vcard:Kind | All | GeoDCAT-AP |
| publisher | M | 1 | dct:publisher | foaf:Organization | All | DCAT-AP |
| dataset | M | N | dcat:dataset | dcat:Dataset | All | DCAT-AP |
| identifier | R | 1 | dct:identifier | rdfs:Literal | MM, MS | DCAT-AP-JRC |
| acronym | R | 1 | dct:alternative | rdfs:Literal | DD, MM | DCAT-AP-JRC |
| landing page | R | 1 | foaf:homepage | foaf:Document | All | DCAT-AP | 
| other identifier | R | N | owl:sameAs | owl:Thing | DD, DC | DCAT-AP-JRC |
| | | | adms:identifier | adms:Identifier | | |
| collection | O | N | dct:hasPart | dcat:Catalog | All | DCAT-AP |
| catalogue | record | O | N | dcat:record | dcat:CatalogRecord | MM, MS | DCAT-AP |

### Dataset

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| identifier | M | 1 | dct:identifier | rdfs:Literal | All | DACT-AP |
| title | M | 1* | dct:title | rdfs:Literal | All | DACT-AP |
| description | M | 1* | dct:description | rdfs:Literal | All | DCAT-AP |
| publication date | M | 1 | dct:issued | rdfs:Literal | All | DCAT-AP |
| bibliographic citation | M | 1 | dct:bibliographicCitation | rdfs:Literal | DC | DCAT-AP-JRC |
| update frequency | M | 1 | dct:accrualPeriodicity | skos:Concept | All | DCAT-AP | 
| theme | M | N | dcat:theme | skos:Concept | All | DCAT-AP |
| contact point | M | 1 | dcat:contactPoint | vcard:Kind | All | DCAT-AP |
| individual contributor | M | N | dct:creator | foaf:Person | DD, DC | GeoDCAT-AP |
| corporate contributor | M | N | dct:creator | foaf:Organization | DD, DC | GeoDCAT-AP |
| contributor(s) | M | 1 | dc:creator | rdfs:Literal | DD, DC | DCAT-AP-JRC |
| publisher | M | 1 | dct:publisher | foaf:Organization | All | DCAT-AP |
| other identifier | R | N | owl:sameAs | owl:Thing | DD, DC | DCAT-AP |
| | | | adms:identifier | adms:Identifier | | |
| status | R | 1 | adms:status | skos:Concept | All | StatDCAT-AP |
| modification date | R | 1 | dct:modified | rdfs:Literal | All | DCAT-AP |
| keyword | R | N | dcat:keyword | rdfs:Literal | All | DCAT-AP |
| temporal coverage | R | N | dct:temporal | dct:PeriodOfTime | All | DCAT-AP |
| spatial coverage | R | N | dct:spatial | dct:Location | All | DCAT-AP |
| landing page | R | 1 | dcat:landingPage | foaf:Document | All | DCAT-AP |
| distribution | R | N | dcat:distribution | dcat:Distribution | All | DCAT-AP |
| publication | R | N | dct:isReferencedBy | foaf:Document | DD | DCAT-AP-JRC |
| language | O | N | dct:language | dct:LinguisticSystem | All | DCAT-AP |
| lineage | O | N | dct:provenance | dct:ProvenanceStatement | DD | DCAT-AP |
| usage notes | O | N | vann:usageNote | foaf:Document | DD | DCAT-AP-JRC |
| input data | O | N | dct:source | dcat:Dataset | DD | DCAT-AP |
| other resource | O | N | dct:relation | owl:Thing | DD | DCAT-AP |
| catalogue | O | 1 | dct:isPartOf | dcat:Catalog | MM, MS | DCAT-AP-JRC |
| conforms to | O | N | dct:conformsTo | dct:Standard | DD, MM, MS | DCAT-AP |
| data policy | O | N | dct:conformsTo | dct:Policy | DD, MM, MS | DCAT-AP-JRC |

### Class: Contact point

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| contact email | C | 1 | vcard:hasEmail | owl:Thing | All | GeoDCAT-AP |
| contact page | C | 1 | vcard:hasURL | owl:Thing | All | GeoDCAT-AP |

### Class: Agent

#### Class: Individual

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| given name | M | 1 | foaf:givenName | rdfs:Literal | DD, DC | DCAT-AP-JRC |
| family name | M | 1 | foaf:familyName | rdfs:Literal | DD, DC | DCAT-AP-JRC |
| formatted name | M | 1 | foaf:name | rdfs:Literal | DD, DC | DCAT-AP-JRC |
| identifier | R | 1 | dct:identifier | rdfs:Literal | MM, MS | DACT-AP-JRC |
| other identifier | R | N | owl:samesAs | owl:Thing | DD, DC | DACT-AP-JRC |
| | | | adms:identifier | adms:Identifier | | |
| email | O | 1 | foaf:mbox | owl:Thing | DD | GeoDCAT-AP |

#### Class: Organisation - Mandatory

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| name | M | 1* | foaf:name | rdfs:Literal | DD | DACT-AP-JRC |
| identifier | R | 1* | dct:identifier | rdfs:Literal | MM, MS | DACT-AP-JRC |
| acronym | R | 1* | dct:alternative | rdfs:Literal | DD | DACT-AP-JRC |
| home page | R | 1* | foaf:homepage | foaf:Document | DD | DACT-AP-JRC |
| other identifier | R | N | owl:samesAs | owl:Thing | DD, DC | DACT-AP-JRC |
| | | | adms:identifier | adms:Identifier | | |
| parent organization | R | 1* | org:subOrganizationOf | foaf:Organization | DD | DACT-AP-JRC |
| logo | O | 1* | foaf:logo | owl:Thing | DD | DACT-AP-JRC |

## Recommended Classes

### Class: Distribution

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| title | M | 1* | dct:title | rdfs:Literal | All | DACT-AP |
| licence | M | 1 | dct:license | dct:LicenseDocument | All | DACT-AP |
| access restrictions | M | 1 | dct:accessRight | dct:RightsStatement | All | DACT-AP-JRC |
| format | M | 1 | dct:format | dct:MediaTypeOrExtent | All | DACT-AP |
| type | C | 1 | dct:type | skos:Concept | All | StatDACT-AP |
| fucntion | C | 1 | dct:type | skos:Concept | All | DCAT-AP-JRC |
| conform to | C | 1 | dct:confirmTo | dct:Standard | All | DACT-AP |
| access URL | C | 1 | dcat:accessURL | owl:Thing | All | DACT-AP |
| download URL | C | 1 | dcat:downloadURL | owl:Thing | All | DACT-AP |
| description | R | 1* | dct:description | rdfs:Literal | All | DACT-AP |

### Class: Publication

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| title | M | 1* | dct:title | rdfs:Literal | DD | DACT-AP-JRC |
| individual author | C | N | dct:creator | foaf:Person | DD | DCAT-AP-JRC |
| corporate author | C | N | dct:creator | foaf:Organization | DD | DCAT-AP-JRC |
| author(s) | M | 1 | dc:creator | rdfs:Literal | DD | DCAT-AP-JRC |
| publisher | M | 1 | dct:publisher | foaf:Organization | DD | DCAT-AP-JRC |
| publisher(s) | M | 1 | dc:publisher | rdfs:Literal | DD | DCAT-AP-JRC |
| publication year | M | 1 | dct:issued | rdfs:Literal | DD | DCAT-AP-JRC |
| identifier | R | 1 | dct:identifier | rdfs:Literal | DD | DCAT-AP-JRC |
| other identifier | R | N | owl:sameAs | owl:Thing | DD | DCAT-AP-JRC |
| |  |  | adms:identifier | adms:Identifier |  |  |
| bibliographic citation | R | 1 | dct:bibliographicCitation | rdfs:Literal | DD | DCAT-AP-JRC |
| type | C | 1 | dct:type | skos:Concept | All | StatDACT-AP |
| abstract | O | 1* | dct:abstract | rdfs:Literal | DD | CAT-AP-JRC |

## Optional Classes

### Class:Endpoint

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| title | M | 1* | dct:title | rdfs:Literal | MM, MS | GeoDCAT-AP |
| contact point | M | 1 | dcat:contactPoint | vcard:Kind | MM, MS | GeoDCAT-AP | 
| publisher | M | 1 | dct:publisher | foaf:Organization | MM, MS | GeoDCAT-AP |
| catalogue | M | 1 | dct:isPartOf | dcat:Catalog | MM, MS | GeoDCAT-AP |
| dataset | M | N | dcat:dataset | dcat:Dataset | MM, MS | GeoDCAT-AP |
| protocol | M | 1 | dct:conformsTo | dct:Standard | MM, MS | DCAT-AP-JRC |
| supported schema | M | N | adms:supportedSchema | adms:Asset | MM, MS | DCAT-AP-JRC |
| access URL | M | 1 | dcat:accessURL | foaf:Document | MM, MS | GeoDCAT-AP |
| identifier | R | 1 | dct:identifier | rdfs:Literal | MM, MS | DACT-AP-JRC |
| acronym | R | 1 | dct:alternative | rdfs:Literal | MM, MS | DACT-AP-JRC  |
| description | R | 1* | dct:description | rdfs:Literal | MM, MS | GeoDCAT-AP |
| collection | O | 1 | dct:isPartOf | dcat:Catalog | MM, MS | DCAT-AP-JRC |
| catalogue record | O | N | dcat:record | dcat:CatalogRecord | MM, MS | GeoDCAT-AP |

### Class: Collection
| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| title | M | 1* | dct:title | rdfs:Literal | All | DCAT-AP |
| description | M | 1* | dct:description | rdfs:Literal | All | DCAT-AP |
| contact point | M | 1 | dcat:contactPoint | vcard:Kind | All | GeoDCAT-AP | 
| publisher | M | 1 | dct:publisher | foaf:Organization | All | DCAT-AP |
| dataset | M | N | dcat:dataset | dcat:Dataset | All | DCAT-AP |
| identifier | R | 1 | dct:identifier | rdfs:Literal | All| DACT-AP-JRC |
| acronym | R | 1 | dct:alternative | rdfs:Literal | All | DACT-AP-JRC  |
| landing page | R | 1* | foaf:homepage | foaf:Document | All | DACT-AP |
| other identifier | R | N | owl:samesAs | owl:Thing | DC | DACT-AP-JRC |
| | | | adms:identifier | adms:Identifier | | |
| catalogue record | O | N | dcat:record | dcat:CatalogRecord | All | DCAT-AP |

### Class: Catalogue record
| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| primary topic | M | 1 | foaf:primaryTopic | owl:Thing | MM, MS | DCAT-AP |
| modification date | M | 1 | dct:modified | rdfs:Literal | MM, MS | DCAT-AP |
| status | M | 1 | adms:status | skos:Concept | MM, MS | DCAT-AP-JRC |
| contact point | M | 1 | dcat:contactPoint | vcard:Kind | MM, MS | GeoDCAT-AP | 
| identifier | R | 1 | dct:identifier | rdfs:Literal | MM, MS | GeoDCAT-AP |
| listing date | R | 1 | dct:created | rdfs:Literal | MM, MS | DCAT-AP |
| metadata schema | R | N | dct:conformsTo | dct:Standard | MM, MS | DCAT-AP |
| source metadata record | R | 1 | dct:source | dcat:CatalogRecord | MM, MS | DCAT-AP |

### Class: Other resource

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| title | M | 1* | dct:title | rdfs:Literal | DD | DCAT-AP-JRC |
| access URL | C | 1 | dcat:accessURL | owl:Thing | DD | DCAT-AP-JRC |
| download URL | C | 1 | dcat:downloadURL | owl:Thing | DD | DCAT-AP-JRC |
| description | R | 1* | dct:description | rdfs:Literal | DD | DCAT-AP-JRC |
| format | R| 1 | dct:format | dct:MediaTypeOrExtent | DD | DCAT-AP-JRC |
| licence | O | 1 | dct:license | dct:LicenseDocument | DD | DCAT-AP-JRC |
| access restrictions | O | 1 | dct:accessRight | dct:RightsStatement | DD | DCAT-AP-JRC |

## Secondary classes

### Class: Lineage

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| label | M | 1 | rdfs:label | rdfs:Literal | DD | GeoDCAT-AP |

### Class: Usage notes

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| label | M | 1 | rdfs:label | rdfs:Literal | DD | DCAT-AP-JRC |

### Class: Identifier

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| notation | M | 1 | skos:notation | rdfs:Literal | DD, DC | DCAT-AP |
| scheme agency name | M | 1 | adms:schemeAgency | rdfs:Literal | DD, DC | DCAT-AP |
| issue date | R | 1 | dct:issued | rdfs:Literal | DD, DC | DCAT-AP |
| scheme agency (as URI) | O | 1 | dct:creator | owl:Thing | DD, DC | DCAT-AP | 

### Class: Temporal coverage

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| start date | C | 1 | schema:startDate | rdfs:Literal | DD | DCAT-AP |
| end date | C | 1 | schema:endDate | rdfs:Literal | DD | DCAT-AP |

### Class: Spatial coverage

#### Spatial coverage as geographical name

#### Spatial coverage as geometry

| Label | Obl. | MC | RDF Property | Range | Scope | Spec. |
|-------|------|----|--------------|-------|-------|-------|
| geometry | C | locn:geometry | gsp:asWKT, gsp:asGML | DD | DCAT-AP |

## References
[DCAT-AP-JRC: An extension to DCAT-AP for multidisciplinary research data](https://ec-jrc.github.io/dcat-ap-jrc/)