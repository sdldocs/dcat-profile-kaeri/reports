> **Note**
>
> 이 페이지는 [FOAF](https://en.wikipedia.org/wiki/FOAF)를 편역하였다.

FOAF(`friend of a friend`의 약자)는 사람, 활동, 다른 사람 및 객체와의 관계를 설명하는 기계 판독 가능한 온톨로지이다. 누구나 FOAF를 사용하여 자신을 설명할 수 있다. FOAF를 사용하면 중앙화된 데이터베이스 없이도 여러 사람들에 대한 소셜 네트워크를 설명할 수 있다.

FOAF는 [Resource Description Framework](RDF)](https://en.wikipedia.org/wiki/Resource_Description_Framework)와 웹 온톨로지 언어(OWL)를 사용하여 표현된 기술(descriptive) 어휘이다. 예를 들어 컴퓨터는 이러한 FOAF 프로필을 사용하여 유럽에 거주하는 모든 사람을 찾거나 나와 내 친구가 알고 있는 모든 사람을 나열할 수 있다. 이는 사람들 간의 관계를 정의함으로써 이루어진다. 각 프로필에는 고유 식별자(예: 해당 사용자의 이메일 주소, 국제 전화번호, Facebook 계정 이름, Jabber ID 또는 해당 사용자의 홈페이지 또는 웹로그의 URI)가 있으며, 이러한 식별자는 이러한 관계를 정의할 때 사용된다.

Libby Miller와 Dan Brickley가 FOAF 프로필의 어휘를 정의하고 확장하는 FOAF 프로젝트fmf 2000년에 시작하였다. 이는 RDF 기술과 '소셜 웹' 문제를 결합한다는 점에서 최초의 소셜 시맨틱 웹 어플리케이션으로 간주될 수 있다.

Tim Berners-Lee는, 2007 년 에세이에서 시맨틱 웹 개념을 네트워크와 문서를 초월하는 관계가 있는 거대한 글로벌 그래프 (GGG: Giant Global Graph)로 재정의했다. 그는 GGG를 인터넷 및 월드와이드웹과 동등한 위치에 있다고 생각하며, "나는 내 네트워크를 FOAF 파일로 표현하며, 이것이 바로 혁명의 시작"이라고 말했다.

## WebID
FOAF는 WebID 사양의 핵심 구성 요소 중 하나이며, 특히 이전 FOAF+SSL로 알려졌던 WebID+TLS 프로토콜의 경우 더욱 그렇다.

## Deployment
비교적 간단한 사용 사례이자 표준이지만, FOAF는 웹에서 제한적으로 채택되어 왔다. 예를 들어, Live Journal과 DeadJournal 블로그 사이트는 모든 회원의 FOAF 프로필을 지원하며, My Opera 커뮤니티는 회원뿐만 아니라 그룹에 대해서도 FOAF 프로필을 지원한다. 현재 Identi.ca, FriendFeed, WordPress, TypePad 서비스에서 FOAF를 지원한다.

Yandex 블로그 검색 플랫폼은 FOAF 프로필 정보에 대한 통한 검색을 지원한다. 사파리 웹 브라우저에서는 사파리 6에서 RSS 지원이 제거되기 전, Firefox 브라우저용 시맨틱 레이더 플러그인에서 대표적인 클라이언트 측 FOAF를 지원하였다. MediaWiki의 시맨틱 주석과 링크 데이터 확장인 Semantic MediaWiki는 기본적으로 활성화된 FOAF를 포함하여 외부 온톨로지에 대한 속성 매핑을 지원한다.

또한 콘텐츠 관리 시스템뿐만 아니라 프로그래밍 언어용 FOAF 프로필 또는 FOAF+SSL 인증을 지원하는 모듈 또는 플러그인도 있다.

## 예
(Turtle 형식으로 작성된) 다음 FOAF 프로필에 따르면 James Wales는 여기에 설명된 인물의 이름이다. 그의 이메일 주소, 홈페이지 및 묘사는 웹 리소스이므로 각각 RDF를 사용하여 설명할 수 있다. 그는 위키미디어를 관심사로 가지고 있으며, Angela Beesley('Person' 리소스의 이름)를 알고 있다.

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .

<#JW>
    a foaf:Person ;
    foaf:name "James Wales" ;
    foaf:mbox <mailto:jwales@bomis.com> ;
    foaf:homepage <http://www.jameswales.com> ;
    foaf:nick "Jimbo" ;
    foaf:depiction <http://www.jameswales.com/aus_img_small.jpg> ;
    foaf:interest <http://www.wikimedia.org> ;
    foaf:knows [
        a foaf:Person ;
        foaf:name "Angela Beesley"
    ] .

<http://www.wikimedia.org>
    rdfs:label "Wikimedia" .
```

## Refs
- [An Introduction to FOAF](https://www.xml.com/pub/a/2004/02/04/foaf.html)
