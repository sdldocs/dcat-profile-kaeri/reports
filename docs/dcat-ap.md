# DCAT-AP(DCAT Application Profile for data portals in Europe)

The purpose of [DCAT-AP] is to define a common interchange metadata format for data portals of the EU and of EU Member States. In order to achieve this, [DCAT-AP] defines a set of classes and properties, grouped into mandatory, recommended and optional. Such classes and properties correspond to information on datasets and data catalogues that are shared by many European data portals, aiding interoperability.

The motivation for using [DCAT-AP] as the reference metadata schema is manifold:

- It is the de facto standard metadata interchange format in European data catalogues.
- It is a general purpose metadata profile, easily extensible.
- It is based on a data model (RDF [RDF-CONCEPTS]) supporting effective linking of distributed resources.
- It is not meant to replace domain- / community-specific standards, but to make them interoperable.
- It is complemented by extensions (as [GeoDCAT-AP] and [StatDCAT-AP]) that can be easily re-used and integrated to support domain-specific requirements, whenever needed.



DCAT-AP(DCAT Application Profile for data portals in Europe)는 유럽의 데이터 포털에 DCAT을 적용하기 위한 구체적인 프로파일 모델을 제공한다. 프로파일 모델은 유럽 데이터 포털 사이의 데이터세트 기술 교환을 용이하게 하기 위해 데이터세트의 메타데이터 레코드(metadata record) 를 상세히 정의한다 [[1](#ref_1)].

## References
1. <a name="ref_1">Wendy Carrara et al, "Towards an open government data ecosystem in Europe using common standards", https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/document/towards-open-government-data-ecosystem-europe-using-common-standards</a>
1. [DCAT Application Profile for data portal in Europe Version 2.1.0](https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/solution/dcat-application-profile-data-portals-europe/release/210)
