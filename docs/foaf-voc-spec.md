> **Note**
>
> 이 페이지는 [FOAF Vocabulary Specification](http://xmlns.com/foaf/0.1/#sec-classesandproperties)를 편역하였다.

## FOAF At A Glance
(to be filled)

## FOAF Overview
광범위한 카테고리로 그룹화된 FOAF 용어.

### FOAF Basics

### Personal Info

### Online Accounts / IM

### Projects와 Group

### Documents와 Images

## 소개: FOAF 기초

## 예
다음은 사람을 설명하는 아주 기본적인 문서이다.

```xml
<foaf:Person>
  <foaf:name>Dan Brickley</foaf:name>
  <foaf:mbox_sha1sum>241021fb0e6289f92815fc210f9e9137262c252e</foaf:mbox_sha1sum>
  <foaf:homepage rdf:resource="http://rdfweb.org/people/danbri/" />
  <foaf:img rdf:resource="http://rdfweb.org/people/danbri/mugshot/danbri-small.jpeg" />
</foaf:Person>
```

이 간단한 예는 FOAF의 기본을 소개하고 있다. 기본적으로 "`foaf:name` 속성이 `Dan Brickley`이고 `foaf:mbox_sha1sum` 속성이 `241021fb0e6289f92815fc210f9e9137262c252e`인 `foaf:Person`이 있으며, 이 사람은 `http://rdfweb.org/people/danbri/` 라는 사물과 `foaf:homepage` 관계에 있고 `http://rdfweb.org/people/danbri/mugshot/danbri-small.jpeg` 라는 사물과 `foaf:img` 관계에 있다."라고 설명gks다.

### 기본 아이디어
기본 아이디어는 매우 간단합니다. 사람들이 FOAF 문서 형식으로 정보를 게시하면 기계가 그 정보를 활용할 수 있게 된다. 이러한 파일에 웹의 다른 문서에 대한 "see also" 참조가 포함되어 있다면 오늘날의 하이퍼텍스트 웹을 기계 친화적인 버전으로 만들 수 있다. 컴퓨터 프로그램은 사람이 아닌 기계를 위해 설계된 문서 웹을 샅샅이 뒤져 찾은 정보를 저장하고, 다른 문서에 대한 'see also' 포인터 목록을 유지하고, 보안을 염두에 두고 디지털 서명을 확인하고, 수집한 문서를 기반으로 웹 페이지와 질의응답 서비스를 구축할 수 있게 될 것이다.

그렇다면 'FOAF 문서 형식'이란 무엇일까? FOAF 파일은 텍스트 문서(즉, 유니코드 문서)이다. XML 구문으로 작성되며, Resource Description Framework (RDF)의 규칙을 채택하고 있다. 또한 FOAF 어휘는 다른 곳에 정의된 다른 RDF 어휘와 함께 FOAF 파일에 나타날 수 있는 몇 가지 유용한 구성을 정의한다. 예를 들어, FOAF는 `foaf:Person`, `foaf:Document`, `foaf:Image`와 같은 카테고리('classes')와 함께 이러한 범주의 편리한 속성인 `foaf:name`, `foaf:mbox`(예: 인터넷 사서함), `foaf:homepage` 등을 정의하며, 이러한 카테고리의 구성원 간에 유지되는 몇 가지 유용한 관계 타입도 정의하고 있다. 예를 들어, 한 가지 흥미로운 관계 타입은 `foaf:depiction`이다. 이것은 어떤 것(예: `foaf:Person`)을 `foaf:Image`와 연관시킨다. '누가 어떤 사진에 있는지'에 대한 사진과 목록이 포함된 FOAF 데모는 RDF 문서를 파싱하고 이러한 속성을 활용하는 소프트웨어 도구를 기반으로 한다.

FOAF 어휘의 구체적인 내용은 이 [FOAF 네임스페이스 문서](http://xmlns.com/foaf/0.1/)에 자세히 설명되어 있다. FOAF 어휘 외에도 FOAF 파일의 가장 흥미로운 기능 중 하나는 다른 FOAF 파일에 대한 "see also" 포인터를 포함할 수 있다는 것이다. 이는 자동 수집 도구가 상호 연결된 파일의 웹을 가로지르며 새로운 사람, 문서, 서비스, 데이터에 대해 학습할 수 있는 기반을 제공한다.

이 사양의 나머지 부분에서는 FOAF의 구문(파일 형식)과 용어에 RDF/XML을 사용하여 웹에 이와 같은 설명을 게시하고 해석하는 방법을 설명한다. 여기에는 여러 카테고리(예: 'Person' 등의 RDF 클래스)와 속성(예: 'mbox' 또는 'workplaceHomepage' 등의 관계 및 속성 타입)이 소개되어 있다. 각 용어 정의는 사람이 읽을 수 있는 형태와 기계가 읽을 수 있는 형태로 제공되며, 빠른 참조를 위해 하이퍼링크로 연결되어 있다.

## FOAF의 용도
FOAF에 대한 일반적인 소개는 Edd Dumbill의 글, [XML Watch: Finding friends with XML and RDF](http://www-106.ibm.com/developerworks/xml/library/x-foaf.html)(2002년 6월, IBM developerWorks)를 참고하여라. [이미지 메타데이터와 함께](http://rdfweb.org/2002/01/photo/) FOAF를 사용하는 방법에 대한 정보도 제공된다.

[공동 묘사(co-depiction)](http://swordfish.rdfweb.org/discovery/2001/08/codepict/) 실험은 어휘를 재미있게 사용하는 방법을 보여준다. Jim Ley의 [SVG 이미지 주석 도구](http://www.jibbering.com/svg/AnnotateImage.html)는 상세한 이미지 메타데이터와 함께 FOAF를 사용하는 방법을 보여주며, 웹 브라우저 내에서 이미지 영역에 라벨링하는 도구를 제공한다. FOAF 문서를 만들려면 Leigh Dodd의 [FOAF-a-matic](http://www.ldodds.com/foaf/foaf-a-matic.html) Javascript 도구를 사용할 수 있다. IRC를 통해 FOAF 데이터세트를 쿼리하려면 IRC '커뮤니티 지원 에이전트'인 Edd Dumbill의 [FOAFbot](http://usefulinc.com/foaf/foafbot) 도구를 사용할 수 있다. FOAF와 관련 프로젝트에 대한 자세한 내용은 [rdfweb.org](http://rdfweb.org/)의 [FOAF 프로젝트 홈페이지](http://usefulinc.com/foaf/foafbot)를 참조하시오.

## 배경
FOAF는 FOAF(rdfweb-dev@vapours.rdfweb.org) 메일링 리스트에서 시맨틱 웹 개발자들 간의 협업으로 관리된다. 'FOAF'라는 이름은 '친구의 친구'의 약자인 전통적인 인터넷 사용 방식에서 유래했다.

소셜 네트워크와 웹, 도시 신화, 신뢰와 연결에 대한 우리의 관심을 반영하기 위해 이 이름이 선택되었다. 이 이름은 도시 전설에 대한 문서화와 조사(예: [alt.folklore.urban archive](http://www.urbanlegends.com/) 또는 [snopes.com](http://www.snopes.com/) 참조) 및 기타 FOAF 스토리에서 계속 사용되고 있다. 웹 어휘와 문서 형식에 'FOAF'라는 이름을 사용하는 것은 이러한 이전 용도를 대체하는 것이 아니라 보완하기 위한 것이다. FOAF 문서는 친구의 친구, 그리고 친구의 친구들 간의 특징과 관계, 그리고 그들이 들려주는 이야기를 설명한다.

## FOAF와 표준


## FOAF 어휘 설명

### FOAF의 진화와 확장

### 용어 상태

## FOAF 자동 검색: FOAF 파일 게시 및 연결

## FOAF와 RDF

## FOAF 상호 참조: FOAF 클래스 및 속성 목록
(to be completed)
