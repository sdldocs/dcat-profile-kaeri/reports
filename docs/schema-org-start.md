> **Note**
> 
> 이 페이지는 [Getting started with schema.org using Microdata](https://schema.org/docs/gs.html)를 요약 편역하였다.

대부분의 웹마스터는 페이지의 HTML 태그에 익숙하다. 일반적으로 HTML 태그는 태그에 포함된 정보를 표시하는 방법을 브라우저에 알려준다. 예를 들어, `<h1>아바타</h1>`는 브라우저에 "Avatar"라는 텍스트 문자열을 `<h1>` 형식으로 표시하도록 지시한다. 그러나 HTML 태그는 "Avatar"가 큰 성공을 거둔 3D 영화를 의미할 수도 있고 프로필 사진의 한 종류를 의미할 수도 있는 등 해당 텍스트 문자열이 무엇을 의미하는지에 대한 정보를 제공하지 않으므로 검색 엔진이 사용자에게 관련 콘텐츠를 지능적으로 표시하기가 더 어려워질 수 있다.

Schema.org는 웹마스터가 Google, Microsoft Yahpp! 등 주요 검색 엔진이 이해할 수 있는 방식으로 페이지를 마크업하는 데 사용할 수 있는 공유 어휘 모음을 제공한다.

웹 콘텐츠에 정보를 추가할 때 [Microdata](http://en.wikipedia.org/wiki/Microdata_(HTML)), [RDFa](http://en.wikipedia.org/wiki/RDFa) 또는 [JSON-LD](http://en.wikipedia.org/wiki/JSON-LD) 형식과 함께 [schema.org](https://schema.org/) 어휘를 사용한다. 이 가이드는 웹 페이지에 마크업을 추가하기 시작할 수 있도록 Microdata와 schema.org를 빠르게 학습하는 데 도움을 줄 수 있다.

이 가이드는 [Microdata](http://en.wikipedia.org/wiki/Microdata_(HTML))에 초점을 맞추고 있지만, [schema.org](https://schema.org/) 사이트의 대부분의 예제에는 RDFa와 JSON-LD의 예도 나와 있다. 여기에 소개된 기본 아이디어(타입, 속성 등)는 Microdata 외에도 관련성이 있으므로 예를 살펴보고 세부 사항을 비교해보시기 바란다.

## Microdata를 사용하여 컨텐츠를 마크업하는 방법

### microdata를 사용하는 이유?
웹 페이지에는 사람들이 웹 페이지를 읽을 때 이해할 수 있는 기본 의미가 있다. 하지만 검색 엔진은 해당 페이지에서 논의되는 내용을 이해하는 데 한계가 있다. 웹 페이지의 HTML에 "Hey search engine, this information describes this specific movie, or place, or person, or video"라는 태그를 추가하면 검색 엔진과 기타 어플리케이션이 콘텐츠를 더 잘 이해하고 유용하고 관련성 있는 방식으로 디스플레이할 수 있다. MicroData는 HTML5와 함께 도입된 태그 집합으로, 이를 통해 이러한 작업을 수행할 수 있다.

### itemscope와 itemtype
구체적인 예로 시작하여 보겠다. 영화 예고편 링크, 감독에 대한 정보 등을 포함한 영화 Avatar에 대한 페이지가 있다고 가정해 보자. HTML 코드는 다음과 같을 수 있다.

```html
<div>
 <h1>Avatar</h1>
 <span>Director: James Cameron (born August 16, 1954)</span>
 <span>Science fiction</span>
 <a href="../movies/avatar-theatrical-trailer.html">Trailer</a>
</div>
```

시작하기 위하여, 페이지에서 영화  Avatar에 대한 `"about"`가 있는 섹션을 식별한다. 이를 위하여 다음과 같이 항목에 대한 정보를 둘러싸는 HTML 태그에 `itemscope` 요소를 추가한다.

```html
<div itemscope>
  <h1>Avatar</h1>
  <span>Director: James Cameron (born August 16, 1954) </span>
  <span>Science fiction</span>
  <a href="../movies/avatar-theatrical-trailer.html">Trailer</a>
</div>
```

`itemscope`를 추가하면 `<div>...</div>` 블록에 포함된 HTML이 특정 항목에 관한 것임을 지정하는 것이다.

그러나 어떤 종류의 항목인지 명시하지 않고 논의 중인 항목이 있다고 지정하는 것은 그다지 도움이 되지 않는다. `itemscope` 바로 뒤에 `itemtype` 어트리뷰트을 사용하여 항목의 타입을 지정할 수 있다.

```html
<div itemscope itemtype="https://schema.org/Movie">
  <h1>Avatar</h1>
  <span>Director: James Cameron (born August 16, 1954)</span>
  <span>Science fiction</span>
  <a href="../movies/avatar-theatrical-trailer.html">Trailer</a>
</div>
```

이는 `div`에 포함된 항목이 실제로는 schema.org 타입 계층 구조에 정의된 대로 `Movie`임을 지정한다. 항목 타입은 URL로 제공된다(이 경우 `https://schema.org/Movie`).

### itemprop
영화 Avatar에 대해 검색 엔진에 어떤 추가 정보를 제공할 수 있을까? 영화에는 배우, 감독, 평점과 같은 흥미로운 속성이 있다. 항목의 속성에 레이블을 지정하려면 `itemprop` 어트리뷰트를 사용한다. 예를 들어, 영화의 감독을 식별하려면 감독 이름을 둘러싸는 요소에 `itemprop="director"`를 추가한다. (영화와 연결할 수 있는 모든 속성의 전체 목록은 https://schema.org/Movie 에서 확인할 수 있다.)

```html
<div itemscope itemtype ="https://schema.org/Movie">
  <h1 itemprop="name">Avatar</h1>
  <span>Director: <span itemprop="director">James Cameron</span> (born August 16, 1954)</span>
  <span itemprop="genre">Science fiction</span>
  <a href="../movies/avatar-theatrical-trailer.html" itemprop="trailer">Trailer</a>
</div>
```

`<span>...<span>` 태그를 추가하여 페이지의 해당 텍스트에 `itemprop` 어트리뷰트를 추가했다. `<span>` 태그는 웹 브라우저에서 페이지를 렌더링하는 방식을 변경하지 않으므로 `itemprop`는 사용하기 편리한 HTML 요소이다.

검색 엔진은 이제 `http://www.avatarmovie.com `이 URL일 뿐만 아니라 `James Cameron`이 감독한 공상과학 영화 Avatar의 예고편 URL이라는 것을 이해할 수 있다.

### 포함된 항목
때로는 항목 속성 값이 자체 속성 집합을 가진 다른 항목이 될 수도 있다. 예를 들어, 영화 감독이 `Person` 타입의 항목이고 `Person`에 `name`과 `birthDate` 속성이 있다고 지정할 수 있다. 속성 값이 다른 항목임을 지정하려면 해당 `itemprop` 바로 뒤에 새 `itemscope`를 시작하면 된다.

```html
<div itemscope itemtype ="https://schema.org/Movie">
  <h1 itemprop="name">Avatar</h1>
  <div itemprop="director" itemscope itemtype="https://schema.org/Person">
  Director: <span itemprop="name">James Cameron</span> (born <span itemprop="birthDate">August 16, 1954</span>)
  </div>
  <span itemprop="genre">Science fiction</span>
  <a href="../movies/avatar-theatrical-trailer.html" itemprop="trailer">Trailer</a>
</div>
```

## schema.org 어휘 사용

### schema.org 타입과 속성
모든 웹 페이지가 영화와 사람에 관한 것은 아니다. [위 섹션](#microdata를-사용하는-이유) 에서 설명한 영화와 사람 타입외에도 schema.org에는 다양한 다른 항목 타입이 있으며, 각 항목에는 항목을 설명하는 데 사용할 수 있는 고유한 속성 집합이 있다.

가장 광범위한 항목 타입은 `name`, `description`, `URL` 및 `image`의 네 가지 속성을 가진 [`Thing`](https://schema.org/Thing)이다. 보다 구체적인 타입은 더 광범위한 타입의 속성을 공유한다. 예를 들어 [`Place`](https://schema.org/Place)는 Thing의 보다 구체적인 타입이고, [`LocalBusiness`](https://schema.org/LocalBusiness)는 Plcae의 보다 구체적인 타입이다. 보다 구체적인 항목은 상위 항목의 속성을 상속한다. (실제로 `LocalBusiness`는 Place보다 더 구체적인 타입이고 Organization보다 더 구체적인 타입이므로 두 상위 타입 모두에서 속성을 상속한다.)

다음은 일반적으로 사용되는 항목 타입의 집합이다.

- Creative works: [`CreativeWork`](https://schema.org/CreativeWork), [`Book`](https://schema.org/Book), [`Movie`](https://schema.org/Movie), [`MusicRecording`](https://schema.org/MusicRecording), [`Recipe`](https://schema.org/Recipe), [`TVSeries`](https://schema.org/TVSeries) ...
- Embedded non-text objects: [`AudioObject`](https://schema.org/AudioObject), [`ImageObject`](https://schema.org/ImageObject), [`VideoObject`](https://schema.org/VideoObject)
- [`Event`](https://schema.org/Event)
- [`Organization`](https://schema.org/Organization)
- [`Person`](https://schema.org/Person)
- [`Place`](https://schema.org/Place), [`LocalBusiness`](https://schema.org/LocalBusiness), [`Restaurant`](https://schema.org/Restaurant) ...
- [`Product`](https://schema.org/Product), [`Offer`](https://schema.org/Offer), [`AggregateOffer`](https://schema.org/AggregateOffer)
- [`Review`](https://schema.org/Review), [`AggregateRating`](https://schema.org/AggregateRating)

또한 [모든 항목 타입의 전체 목록](https://schema.org/docs/full.html)을 한 페이지에 나열하여 볼 수도 있다.

### 예상 타입, 텍스트와 URL
다음은 웹 페이지에 schema.org 마크업을 추가할 때 염두에 두어야 할 몇 가지 참고 사항들이다.

- **숨겨진 텍스트를 제외하고는 많을수록 좋다**. 일반적으로 마크업하는 콘텐츠는 많을수록 좋다. 그러나 일반적으로 웹 페이지를 방문하는 사람들에게 표시되는 콘텐츠만 마크업해야 하며 숨겨진 div나 기타 숨겨진 페이지 요소에 있는 콘텐츠는 마크업하지 않아야 한다.
- **예상 타입과 텍스트**. schema.org 타입을 탐색할 때 많은 속성에 "예상 타입"이 있는 것을 알 수 있다. 이는 속성 값 자체가 포함된 항목이 될 수 있음을 의미한다([포함된 항목](#포함된-항목) 참조). 그러나 이는 필수 사항은 아니며 일반 텍스트나 URL만 포함해도 괜찮다. 또한 예상 타입이 지정될 때마다 예상 타입의 하위 타입인 항목을 임베드해도 괜찮다. 예를 들어 예상 타입이 장소인 경우 `LocalBusiness`를 포함해도 괜찮다.
- **URL 속성 사용**. 일부 웹 페이지는 특정 항목에 관한 것이다. 예를 들어 한 사람에 대한 웹 페이지가 있을 수 있으며, 이 웹 페이지는 사람 항목 타입을 사용하여 마크업할 수 있다. 다른 페이지에는 설명된 항목의 컬렉션이 있다. 예를 들어 회사 사이트에 직원을 나열하는 페이지가 있고 각 직원의 프로필 페이지로 연결되는 링크가 있을 수 있다. 이와 같이 항목 컬렉션이 있는 페이지의 경우 각 항목을 개별적으로 마크업하고(이 경우 일련의 사람으로) 각 항목의 해당 페이지 링크에 다음과 같이 URL 속성을 추가해야 한다.

```html
<div itemscope itemtype="https://schema.org/Person">
  <a href="alice.html" itemprop="url">Alice Jones</a>
</div>
<div itemscope itemtype="https://schema.org/Person">
  <a href="bob.html" itemprop="url">Bob Smith</a>
</div>
```

### 마크업 검사
웹 페이지 레이아웃의 변경 사항을 테스트할 때 웹 브라우저가 중요하고 작성한 코드를 테스트할 때 코드 컴파일러가 중요한 것처럼, schema.org 마크업도 테스트하여 올바르게 구현되었는지 확인해야 한다. Google은 마크업을 테스트하고 오류를 식별하는 데 사용할 수 있는 리치 스니펫 테스트 도구(rich snippets testing tool)를 제공한다.

## 고급 주제: 정보의 기계가 이해할 수 있는 버전
많은 페이지는 `itemscope`, `itemtype`과 `itemprop`([Microdata를 사용하여 컨텐츠를 마크업하는 방법](#microdata를-사용하여-컨텐츠를-마크업하는-방법)에 설명되어 있음)과 schema.org에 정의된 타입과 속성([schema.org 어휘 사용](#schemaorg-타입과-속성)에 설명되어 있음)만 사용하여 설명할 수 있다.

그러나 때로는 항목 속성을 추가적으로 명확하게 설명하지 않으면 기계가 이해하기 어려운 경우가 있다. 여기서는 페이지를 마크업할 때 기계가 이해할 수 있는 버전의 정보를 제공하는 방법에 대해 설명한다.

- 날짜, 시간 및 기간: `datetime`과 함께 `time` 태그를 사용한다.
- 열거형 및 정식(canonical) 참조: `href`와 같이 `link` 태그를 사용한다.
- 누락된/암시적 정보: `content`와 함께 `meta` 태그를 사용한다.

### 날짜, 시간 및 기간: `datetime`과 함께 `time` 태그를 사용
날짜와 시간은 기계가 이해하기 어려울 수 있다. "04/01/11"이라는 날짜를 생각해 보자. 2004년 1월 11일을 의미할까? 2011년 1월 4일을 의미할까? 아니면 2011년 4월 1일을 의미할까? 날짜를 모호하지 않게 만들려면 시간 태그와 날짜/시간 속성을 함께 사용하면 된다. 날짜/시간 속성의 값은 `YYYY-MM-DD` 형식을 사용하여 지정된 날짜이다. 아래 HTML 코드는 날짜를 2011년 4월 1일로 명확하게 지정한다.

```html
<time datetime="2011-04-01">04/01/11</time>
```

`hh:mm` 또는 `hh:mm:ss` 형식을 사용하여 하루 내의 시간을 지정할 수도 있다. 시간 앞에는 문자 `T`가 붙으며 다음과 같이 날짜와 함께 입력할 수 있다.

```html
<time datetime="2011-05-08T19:30">May 8, 7:30pm</time>
```

문맥을 살펴보자. 다음은 2011년 5월 8일에 열리는 콘서트를 설명하는 HTML이다. 이벤트 마크업에는 이벤트 이름, 설명 및 이벤트 날짜가 포함된다.

```html
<div itemscope itemtype="https://schema.org/Event">
  <div itemprop="name">Spinal Tap</div>
  <span itemprop="description">One of the loudest bands ever
  reunites for an unforgettable two-day show.</span>
  Event date:
  <time itemprop="startDate" datetime="2011-05-08T19:30">May 8, 7:30pm</time>
</div>
```

기간은 `datetime` 속성이 있는 `time` 태그를 사용하여 비슷한 방식으로 지정할 수 있다. 기간 앞에는 문자 `P`('기간'을 의미)가 붙는다. 레시피 조리 시간을 1시간 30분으로 지정하는 방법은 다음과 같다.

```html
<time itemprop="cookTime" datetime="PT1H30M">1 1/2 hrs</time>
```

`H`는 시간을 지정하는 데 사용되며, `M`은 분을 지정하는 데 사용한다.

날짜, 시간 및 기간 표준은 [ISO 8601 날짜/시간 표준](http://en.wikipedia.org/wiki/ISO_8601)에 따라 지정된다.

### 열거형 및 표준(canonical) 참조: `href`와 같이 `link` 태그를 사용

#### 열거형
일부 속성은 가능한 값의 제한된 집합만 사용할 수 있다. 프로그래머는 이를 흔히 "열거형(enumerations)"이라고 부른다. 예를 들어, 판매 품목이 있는 온라인 스토어에서는 [`Offer`](https://schema.org/Offer) 품목 타입을 사용하여 오퍼의 세부 정보를 지정할 수 있다. 가용성 속성은 일반적으로 `In stock`, `Out of stock`, `Preorder` 등 몇 가지 가능한 값 중 하나만 가질 수 있다. 항목 타입이 URL로 지정되는 것과 마찬가지로 schema.org의 열거형에 대해 가능한 값도 URL로 지정할 수 있다.

다음은 오퍼 타입과 관련 속성으로 마크업된 판매용 품목이다.

```html
<div itemscope itemtype="https://schema.org/Offer">
  <span itemprop="name">Blend-O-Matic</span>
  <span itemprop="price">$19.95</span>
  <span itemprop="availability">Available today!</span>
</div>
```

그리고 여기에 동일한 항목이 있지만 `link`와 `href`를 사용하여 허용된 값 중 하나로 사용 가능성을 명확하게 지정한다.

```html
<div itemscope itemtype="https://schema.org/Offer">
  <span itemprop="name">Blend-O-Matic</span>
  <span itemprop="price">$19.95</span>
  <link itemprop="availability" href="https://schema.org/InStock"/>Available today!
</div>
```

일반적으로 속성에 대한 일반적인 값의 수가 제한되어 있는 경우 schema.org에 지정된 해당 열거 형이 있다. 이 경우 `availability`에 대한 가능한 값은 [ItemAvailability](https://schema.org/ItemAvailability)에 지정된다.

#### 표준 참조
일반적으로 링크는 `<a>` 요소를 사용하여 지정된다. 예를 들어, 다음 HTML은 "Catcher in the Rye"이라는 책의 Wikipedia 페이지로 연결된다.

```html
<div itemscope itemtype="https://schema.org/Book">
  <span itemprop="name">The Catcher in the Rye</span>—
  by <span itemprop="author">J.D. Salinger</span>
  Here is the book's <a itemprop="url" href="http://en.wikipedia.org/wiki/The_Catcher_in_the_Rye">Wikipedia page</a>.
</div>
```

보시다시피 `itemprop="url"`을 사용하여 동일한 항목에 대해 설명하는 다른 사이트(이 경우 Wikipedia)의 페이지로 연결되는 링크를 지정할 수 있다. 서드 파티 사이트 링크는 검색 엔진이 웹 페이지에서 설명하는 항목을 더 잘 이해하는 데 도움이 될 수 있다.

그러나 페이지에 보이는 링크를 추가하고 싶지 않을 수도 있다. 이 경우 다음과 같이 `link` 요소를 대신 사용할 수 있다.

```html
<div itemscope itemtype="https://schema.org/Book">
  <span itemprop="name">The Catcher in the Rye</span>—
  <link itemprop="url" href="http://en.wikipedia.org/wiki/The_Catcher_in_the_Rye" />
  by <span itemprop="author">J.D. Salinger</span>
</div>
```

### 누락된/암시적 정보: `content`와 함께 `meta` 태그를 사용
웹 페이지에 마크업할 가치가 있는 정보가 있지만 페이지에 표시되는 방식 때문에 마크업할 수 없는 경우가 있다. 이 정보는 이미지(예: 평점 5점 만점에 4점을 나타내는 데 사용되는 이미지) 또는 플래시 객체(예: 동영상 클립의 길이)로 전달되거나, 페이지에 명시적으로 명시되어 있지 않지만 암시적으로 전달될 수 있다(예: 가격의 통화).

이러한 경우 콘텐츠 속성과 함께 메타 태그를 사용하여 정보를 지정한다. 사용자에게 별점 5점 만점에 4점을 표시하는 이미지를 예로 들어 보겠다.

```html
<div itemscope itemtype="https://schema.org/Offer">
  <span itemprop="name">Blend-O-Matic</span>
  <span itemprop="price">$19.95</span>
  <img src="four-stars.jpg" />
  Based on 25 user ratings
</div>
```

다음은 등급 정보가 표시된 예이다.

```html
<div itemscope itemtype="https://schema.org/Offer">
  <span itemprop="name">Blend-O-Matic</span>
  <span itemprop="price">$19.95</span>
  <div itemprop="reviews" itemscope itemtype="https://schema.org/AggregateRating">
    <img src="four-stars.jpg" />
    <meta itemprop="ratingValue" content="4" />
    <meta itemprop="bestRating" content="5" />
    Based on <span itemprop="ratingCount">25</span> user ratings
  </div>
</div>
```

이 기법은 신중하게 사용해야 한다. 다른 방법으로 마크업할 수 없는 정보에 대해서만 콘텐츠와 함께 메타를 사용하시오.

### schema.org 확장
대부분의 사이트와 조직은 schema.org를 확장할 이유가 없을 것이다. 그러나 schema.org는 기존 타입에 추가 속성이나 하위 타입을 지정할 수 있는 기능을 제공한다. 이 기능에 관심이 있다면 [schema.org 확장 메커니즘](https://schema.org/docs/extension.html)에 대해 자세히 읽어보도록 한다.
